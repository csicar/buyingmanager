import { Meteor } from 'meteor/meteor';
import '../imports/api/tasks.js';
import '../imports/api/products.js';
import '../imports/api/projects.js';
import '../imports/api/requirements.js';

Meteor.startup(() => {
  // code to run on server at startup
});
