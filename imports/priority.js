export function formatPriority(p) {
	if (p <= 0.3) {
		return "unwichtig"
	} else if (p <= 0.6) {
		return "mittel";
	} else {
		return "wichtig"
	}
}