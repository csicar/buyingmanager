import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';
import Requirement from '../ui/Requirement';
import { Requirements } from './requirements.js';
import { Products } from './products.js';

function generatePowerSet(array) {
	var result = [];
	result.push([]);

	for (var i = 1; i < (1 << array.length); i++) {
		var subset = [];
		for (var j = 0; j < array.length; j++)
			if (i & (1 << j))
				subset.push(array[j]);

		result.push(subset);
	}

	return result;
}

function cartesianProduct(arr) {
	return arr.reduce(function (a, b) {
		return a.map(function (x) {
			return b.map(function (y) {
				return x.concat(y);
			})
		}).reduce(function (a, b) { return a.concat(b) }, [])
	}, [[]])
}

// [{products: [...]}, ...]
function generateOptions(requirements) {
	const topLevelOptions = generatePowerSet(requirements);
	const productLevelOptions = [];
	topLevelOptions.forEach(requirementsSet => {
		const productOptions = requirementsSet.map(it => it.products);
		const productSelection = cartesianProduct(productOptions)
		productSelection.forEach(it => {
			productLevelOptions.push(it);
		})
	})
	return productLevelOptions;
}

function selectBestOptions(requirements, budget) {
	const options = generateOptions(requirements)
	options.forEach(it => {
		it.costSum = it.map(a => a.cost).reduce((a, b) => a+b, 0);
		it.qualitySum = it.map(a => a.quality).reduce((a, b) => a+b, 0);
	})
	const validOptions = options.filter(it => it.costSum <= budget)
	validOptions.sort((a, b) => {
		return b.qualitySum - a.qualitySum;
	})
	return validOptions;
}

export const Projects = new Mongo.Collection('projects');

Meteor.methods({
	'projects.insert'(name, budget) {
		check(name, String);
		check(budget, Number);

		// Make sure the user is logged in before inserting a task
		if (!this.userId) {
			throw new Meteor.Error('not-authorized');
		}

		Projects.insert({
			name,
			budget,
			createdAt: new Date(),
			owner: this.userId,
			username: Meteor.users.findOne(this.userId).username,
		});
	},
	'projects.magic_allocate'(projectId) {
		if (!this.userId) {
			throw new Meteor.Error('not-authorized');
		}

		const { budget } = Projects.findOne(projectId);
		const requirements = Requirements.find({projectId});
		const resolutionOptions = [];
		requirements.forEach(req => {
			resolutionOptions.push({
				products: Products
					.find({requirement:req._id})
					.fetch()
					.map(it => Object.assign(it, {quality: req.priority*it.priority})),
				req:req
			})
		})
		console.log(resolutionOptions, resolutionOptions.map(it =>it.products));
		const bestOption = selectBestOptions(resolutionOptions, budget)[0];
		
		//foreach options find requirement and update
		bestOption.forEach(product => {
			Requirements.update({_id: product.requirement}, { $set: { resolutionId: product._id}});
		})
	},
	'projects.remove'(projectId) {
		check(projectId, String);

		if (!this.userId) {
			throw new Meteor.Error('not-authorized');
		}

		Projects.remove(projectId);
	},
});