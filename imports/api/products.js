import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';
import { Requirements } from './requirements.js';
import Images from './images.js';

const fs = require('fs');
export const Products = new Mongo.Collection('products');

Meteor.methods({
	'products.insert'(name, cost, priority, url, requirement, fileId) {
		check(name, String);
		check(cost, Number);
		check(priority, Number);
		check(url, String);
		check(requirement, String);

		// Make sure the user is logged in before inserting a task
		if (!this.userId) {
			throw new Meteor.Error('not-authorized');
		}
		const image = fileId?Images.findOne(fileId).link():null;

		Products.insert({
			name,
			cost,
			priority,
			url,
			requirement,
			createdAt: new Date(),
			image
		});
	},
	'products.update'(product, fileId) {
		const {name, cost, priority, url, _id} = product;
		check(name, String);
		check(cost, Number);
		check(priority, Number);
		check(url, String);
		check(_id, String);

		if (!this.userId) {
			throw new Meteor.Error('not-authorized');
		}

		if (fileId) {

			const link = Images.findOne(fileId).link();
			console.log(link);
			Products.update({_id}, {$set: {name, cost, priority, url, image: link}});
		}
		Products.update({ _id }, { $set: { name, cost, priority, url } });

	},
	'products.remove'(productId) {
		check(productId, String);

		if (!this.userId) {
			throw new Meteor.Error('not-authorized');
		}
		
		Requirements.update({resolutionId: productId}, {$set: {resolutionId: null}});
		Products.remove(productId);
	},
});