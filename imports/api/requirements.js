import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Requirements = new Mongo.Collection('requirements');

Meteor.methods({
	'requirements.insert'(name, priority, projectId) {
		check(name, String);
		check(priority, Number);
		check(projectId, String);

		// Make sure the user is logged in before inserting a task
		if (!this.userId) {
			throw new Meteor.Error('not-authorized');
		}

		Requirements.insert({
			name,
			resolutionId: null,
			priority,
			projectId,
			createdAt: new Date(),
		});
	},
	'requirements.edit'(requirement) {
		console.log(requirement)
		const {name, priority, _id} = requirement
		check(_id, String);
		check(name, String);
		check(priority, Number);

		if (!this.userId) throw new Meteor.Error('not-authorized');

		Requirements.update({_id: requirement._id}, {$set:{name, priority}});
	},
	'requirements.select'(requirementId, productId) {
		check(requirementId, String);
		check(productId, String);
		if (!this.userId) {
			throw new Meteor.Error('not-authorized');
		}
		Requirements.update(requirementId, {$set: {resolutionId: productId}})
	},
	'requirements.remove'(projectId) {
		check(projectId, String);
		
		if (!this.userId) {
			throw new Meteor.Error('not-authorized');
		}

		Requirements.remove(projectId);
	},
});