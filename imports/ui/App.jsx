import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { withTracker } from 'meteor/react-meteor-data';
import { Projects } from '../api/projects.js';
import { Products } from '../api/products.js';
import { Requirements } from '../api/requirements';
import AccountsUIWrapper from './AccountsUIWrapper.jsx';
import Project from './Project.jsx';
import ProjectsOverview from './Projects.jsx';
import { BrowserRouter, Route, Switch, NavLink, Redirect } from 'react-router-dom'
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import { Grid } from '@material-ui/core';
import CircularProgress from '@material-ui/core/CircularProgress';

// App component - represents the whole app
class App extends Component {
	constructor(props) {
		super(props)
		this.state = {
			selectedView: 'projects',
			menuOpen: false,
		}
	} 
	

	createProject(event) {
		event.preventDefault();

		// Find the text field via the React ref
		const text = ReactDOM.findDOMNode(this.refs.textInput).value.trim();

		Meteor.call('projects.insert', text, 10);

		// Clear form
		ReactDOM.findDOMNode(this.refs.textInput).value = '';
	}

	createProduct(event) {
		event.preventDefault();
		const name = ReactDOM.findDOMNode(this.refs.nameInput).value.trim();
		Meteor.call('products.insert', name, 10, "asd");
		
	}

	getProjectById(id) {
		if (!this.props.projects) return {};
		return this.props.projects.find(it => it._id == id);
	}

	renderTasks() {
		return this.props.projects.map((project) => (
			<Project key={project._id} project={project} />
		));
	}

	renderNav() {
		return <AppBar className="mb-5" position="static">
			<Toolbar>
				<IconButton color="inherit" aria-label="Menu">
					<MenuIcon />
				</IconButton>
				<Typography variant="title" color="inherit">
					Budget Manager
				</Typography>
				<Menu open={this.state.menuOpen}>
					<MenuItem>
						<NavLink className="nav-link" to="/projects">Projects</NavLink>
					</MenuItem>
				</Menu>
				<AccountsUIWrapper />
			</Toolbar>
		</AppBar>
	}

	render() {
		return (
			<BrowserRouter><div>
				{this.renderNav()}
				<Grid container spacing={24}>
					<Grid item xs={false} lg={1}></Grid>
					<Grid item xs={12} lg={10}>
						<Switch>
							<Route exact path="/projects" render={(props) => 
								<ProjectsOverview {...props} projects={this.props.projects}/>
							}/>
							<Route exact path="/projects/:projectId" render={(props) => {
								if (this.props.products && this.props.projects) {

									const project = this.getProjectById(props.match.params.projectId);
									if (!project) return <div><CircularProgress /></div>;
									const {products, requirements} = this.props
									return <Project {... props} products={products} project={project} requirements={requirements} />
								}
								return <div><CircularProgress/></div>
							}}/>
							<Route exact path="/" render={props => 
								<Redirect to="/projects"/>
							}/>
						</Switch>
					</Grid>
				</Grid>
			</div></BrowserRouter>
		);
	}
}

export default withTracker(() => {
	return {
		projects: Projects.find({}).fetch(),
		products: Products.find({}).fetch(),
		requirements: Requirements.find({}).fetch(),
		currentUser: Meteor.user(),
	};
})(App);
