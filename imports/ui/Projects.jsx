import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { SketchPicker } from 'react-color';
import { Projects } from '../api/tasks.js';
import { Meteor } from 'meteor/meteor';
import { Link } from 'react-router-dom'
import { List, ListItem, ListItemText } from '@material-ui/core';

var { EditableInput } = require('react-color/lib/components/common');

// Task component - represents a single todo item

export default class Project extends Component {
	deleteThisProject() {
		Meteor.call('projects.remove', this.props.project._id)
	}

	addProject(ev) {
		ev.preventDefault();

		const name = ReactDOM.findDOMNode(this.refs.nameInput).value.trim();
		const budget = ReactDOM.findDOMNode(this.refs.budgetInput).value.trim();

		Meteor.call('projects.insert', name, +budget)
	}

	renderProjects() {
		return this.props.projects.map((project) =>
			(<ListItem key={project._id}>
				<ListItemText primary={
					<Link to={`/projects/${project._id}`}>
						{project.name} mit Budget {project.budget}€ 
					</Link>
				}/>
			</ListItem>
		)
		);
	}

	render() {
		return (
			<div>
				<form className="mb-4" onSubmit={this.addProject.bind(this)}>
					<input className="mx-2" type="text" ref="nameInput" placeholder="Name"/>
					<input className="mx-2" type="number" ref="budgetInput" placeholder="Budget"/>
					<button className="btn btn-primary mx-2" type="submit">erstellen</button>
				</form>
				<List>
					
					{this.renderProjects()}

				</List>
			</div>
		);
	}
}

