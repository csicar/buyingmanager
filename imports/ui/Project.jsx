import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Requirement from './Requirement.jsx';
import { Meteor } from 'meteor/meteor';
import { Link } from 'react-router-dom';
import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from '@material-ui/icons/Delete';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import { Card, CardHeader, CardActions, CardContent, Grid, IconButton, Icon, Input, InputLabel, Select, Typography, MenuItem, TextField } from '@material-ui/core';

// Task component - represents a single todo item

export default class Project extends Component {
	constructor(props) {
		super(props)
		this.state = {
			priority: 0,
			name: "",
		}
	}

	deleteThisProject() {
		Meteor.call('projects.remove', this.props.project._id)
	}

	addRequirement() {
		Meteor.call('requirements.insert', this.state.name, this.state.priority, this.props.project._id)
	}

	magicAllocate() {
		Meteor.call('projects.magic_allocate', this.props.project._id);
	}

	showEditDialog() {
		this.setState({editDialog: true})
	}

	handleChange(attr) {
		return (event) => {
			this.setState({ [attr]: event.target.value});
		}
	}

	getSum() {
		return this.getRequirements()
			.map(it => it.resolutionId)
			.filter(it => !!it)
			.map(it => this.props.products.find(p => p._id === it))
			.filter(it => !!it)
			.map(it => it.cost)
			.reduce((a, b) => a + b, 0)
	}

	getRequirements() {
		const {project, requirements} = this.props
		if (!project || !requirements) return []
		return requirements.filter(it => it.projectId === project._id);
	}

	renderRequirements() {
		return this.getRequirements().map((requirement) =>
			(<Grid item xs={12} sm={6} lg={4} key={requirement._id} className="my-3">
				<Requirement requirement={requirement} products={this.props.products} />
			</Grid>)
		);
	}

	render() {
		return (
			<Grid container spacing={24}>
				<Grid item xs={12}>
					<Grid container alignItems="baseline" spacing={24}>
						<Grid item xs onDoubleClick={this.showEditDialog.bind(this)} style={{flexBasis: 'auto', flexGrow: 0}}>
							<Typography variant="display2">
								Projekt {this.props.project.name}
							</Typography>
						</Grid>
						<Grid item xs>
							<Typography variant="subheading">
								{this.getSum()}€
						</Typography>
						</Grid>
						<Grid item xs>
							<IconButton onClick={this.deleteThisProject.bind(this)}>
								<DeleteIcon />
							</IconButton>
						</Grid>
					</Grid>
				</Grid>
				<Grid item xs={12} sm={6} lg={4}>
					<Card elevation={5}>
						<CardHeader title="Neue Produktgruppe"/>
						<CardContent>
							<form>
								<TextField
									id="nameInput"
									label="Was?"
									placeholder="Tisch"
									onChange={this.handleChange('name').bind(this)}
								/>
								<FormControl>
									<InputLabel htmlFor="priority-input">Priorität</InputLabel>
									<Select
										value={this.state.priority}
										onChange={this.handleChange('priority').bind(this)}
										input={<Input name="priority" id="priority-input" />}
									>
										<MenuItem value={0}>
											<em>None</em>
										</MenuItem>
										<MenuItem value={0.3}>Unwichtig</MenuItem>
										<MenuItem value={0.6}>Mittel</MenuItem>
										<MenuItem value={1}>Wichtig</MenuItem>
									</Select>
								</FormControl>
							</form>
						</CardContent>
						<CardActions style={{justifyContent: "flex-end"}}>
							<Button variant="raised" color="primary" onClick={this.addRequirement.bind(this)}>
								<AddIcon />
								Hinzufügen
							</Button>
						</CardActions>
					</Card>
				</Grid>
				{this.renderRequirements()}
			</Grid>
		);
	}
}

