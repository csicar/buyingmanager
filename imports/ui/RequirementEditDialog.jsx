import { withMobileDialog } from "@material-ui/core";
import { FormControl, Input, Grid, Dialog, DialogTitle, DialogContent, DialogActions, Select, MenuItem, TextField, InputLabel, Button } from '@material-ui/core';
import React, { Component } from 'react';


class RequirementEditDialog extends Component {
	constructor(props) {
		super(props)
		this.state= {
			requirement: Object.assign({}, this.props.model),
		}
		console.log(this.state.requirement)
	}

	handleSave() {
		this.props.handleChange(this.state.requirement)
	}

	handleChange(prop) {
		return (e) => {
			this.state.requirement[prop] = e.target.value
			this.setState(this.state)
		}
	}

	render() {
		return <Dialog
			fullScreen={this.props.fullScreen}
			open={this.props.dialogOpen}
			onClose={this.props.closeDialog}
		>
			<DialogTitle id="form-dialog-title">Edit Requirement</DialogTitle>
			<DialogContent>
				<TextField
					id="nameInput"
					label="Was?"
					placeholder="Tisch"
					value={this.state.requirement.name}
					onChange={this.handleChange('name').bind(this)}
				/>
				<FormControl>
					<InputLabel htmlFor="priority-input">Priorität</InputLabel>
					<Select
						value={this.state.requirement.priority}
						onChange={this.handleChange('priority').bind(this)}
						inputProps={{
							name: 'priority',
							id: 'priority-input',
						}}
					>
						<MenuItem value={0}>
							<em>None</em>
						</MenuItem>
						<MenuItem value={0.3}>Unwichtig</MenuItem>
						<MenuItem value={0.6}>Mittel</MenuItem>
						<MenuItem value={1}>Wichtig</MenuItem>
					</Select>
				</FormControl>
			</DialogContent>
			<DialogActions>
				<Button onClick={this.props.handleClose} color="primary">
					Cancel
				</Button>
				<Button onClick={this.handleSave.bind(this)} color="primary">
					Save
				</Button>
			</DialogActions>
		</Dialog>
	}
}

export default withMobileDialog()(RequirementEditDialog)