import React, { Component } from 'react';
import { Tasks } from '../api/tasks.js';
import { Meteor } from 'meteor/meteor';

// Task component - represents a single todo item

export default class Task extends Component {
	deleteThisTask() {
		Meteor.call('tasks.remove', this.props.task._id)
	}
	render() {
		return (
			<li>
				<button className="delete" onClick={this.deleteThisTask.bind(this)}>
					&times;
				</button>
				{this.props.task.text}
				{this.props.task.userId}
			</li>
		);
	}
}

