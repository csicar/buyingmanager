import { Button, Divider } from '@material-ui/core';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ProductForm from './ProductForm.jsx';
import {formatPriority} from '../priority.js';

export default class Product extends Component {
	constructor(props) {
		super(props);
		this.state = {
			editMode: false
		}
	}
	deleteThisProduct() {
		Meteor.call('products.remove', this.props.product._id)
	}

	selectProduct(product) {
		return (ev) => {
			Meteor.call('requirements.select', this.props.requirement._id, product._id);
		}
	}

	toggleEdit(ev) {
		this.setState({
			editMode: !this.state.editMode
		})
	}

	renderInner() {
		const product = this.props.product;
		if (product.url) {
			return <a href={product.url}>{product.name} {product.cost}€ {formatPriority(product.priority)}</a>
		} else {
			return <span>{product.name} {product.cost}€ {formatPriority(product.priority)}</span>
		}
	}

	updateProduct(product,file) {
		console.log(product);
		Meteor.call('products.update', product, file);
		this.toggleEdit();
	}

	renderForm() {
		return <ProductForm asEdit={true} product={this.props.product} onUpdate={this.updateProduct.bind(this)}/>
	}

	render() {
		const {product, requirement} = this.props;
		const style = {};
		if (product._id === requirement.resolutionId) {
			style.backgroundColor = '#80DEEA';
		}
		return (
			<ExpansionPanel>
				<ExpansionPanelSummary style={style} expandIcon={<ExpandMoreIcon />}>
					{this.renderInner()}
				</ExpansionPanelSummary>

				<ExpansionPanelDetails>
					{this.renderForm()}
				</ExpansionPanelDetails>
				<Divider/>
				<ExpansionPanelActions>
					<Button size="small" onClick={this.deleteThisProduct.bind(this)}>Delete</Button>
					<Button size="small" onClick={this.selectProduct(this.props.product).bind(this)} color="primary">
						select
					</Button>
				</ExpansionPanelActions>
			</ExpansionPanel>
		);
	}
}

