import React, { Component } from 'react';
import { Products } from '../api/products.js';
import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import { withMobileDialog, FormControl, Input, Grid, Dialog, DialogTitle, DialogContent, DialogActions, Select, MenuItem } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Dropzone from 'react-dropzone'
import Slider from '@material-ui/lab/Slider';
import TextField from '@material-ui/core/TextField';
import Images from '../api/images.js';

// Task component - represents a single todo item

class ProductForm extends Component {
	constructor(props) {
		super(props);
		const {cost, name, priority, url} = this.props.product;

		this.state = {
			file: null,
			name,
			cost,
			priority,
			url,
			dialogOpen: false,
		}
	}

	openDialog() {
		this.setState({dialogOpen: true})
	}

	closeDialog() {
		this.setState({dialogOpen: false})
	}

	handleFileChange(acceptedFiles) {
		console.log(acceptedFiles);
		this.setState({file: acceptedFiles[0]});
	}

	fireUpdate() {
		const {name, cost, priority, url} = this.state;

		if (this.state.file !== null) {
			const upload = Images.insert({
				file: this.state.file
			}, false)

			upload.on('start', () => {
				console.log('start', this);
			});
			upload.on('end', (err, fileObj) => {
				console.log("emd", fileObj)
				this.props.onUpdate({ name, cost: +cost, priority: +priority, url, _id: this.props.product._id }, fileObj._id);
			})
			upload.start();
		} else {
			this.props.onUpdate({ name, cost: +cost, priority: +priority, url, _id: this.props.product._id }, null);
		}

		this.closeDialog();
	}

	handleChange(name) {
		return (ev) => {
			this.state[name] = ev.target.value
			this.setState(this.state);
		}
	}

	render() {
		return <div>
			<Button onClick={this.openDialog.bind(this)}>{this.props.asEdit?'edit':'add'}</Button>
			<Dialog
				fullScreen={this.props.fullScreen}
				open={this.state.dialogOpen}
				onClose={this.closeDialog}
			>
				<DialogTitle>{this.props.asEdit?'Edit':'Create a'} Product</DialogTitle>
				<DialogContent>
					<form className="form-inline">
						<Grid container>
							<Grid item xs={12}>
								<TextField
									id="nameInput"
									label="Name"
									value={this.state.name}
									onChange={this.handleChange('name').bind(this)}
								/>
							</Grid>
							<Grid item xs={12}>
								<FormControl style={{ minWidth: "200px" }}>
									<InputLabel htmlFor="priority-input">Priorität</InputLabel>
									<Select
										fullWidth={true}
										value={this.state.priority}
										onChange={this.handleChange('priority').bind(this)}
										input={<Input name="priority" id="priority-input" />}
									>
										<MenuItem value={0}>
											<em>None</em>
										</MenuItem>
										<MenuItem value={0.3}>Unwichtig</MenuItem>
										<MenuItem value={0.6}>Mittel</MenuItem>
										<MenuItem value={1}>Wichtig</MenuItem>
									</Select>
								</FormControl>
							</Grid>
							<Grid item xs={12}>
								<TextField
									id="urlInput"
									label="Link"
									value={this.state.url}
									onChange={this.handleChange('url').bind(this)}
									margin="normal"
								/>
							</Grid>
							<Grid item xs={12}>
								<Dropzone
									onDrop={this.handleFileChange.bind(this)}
									accept="image/*"
									style={{ height: "100px" }}
								>
									<Button variant="contained" color="primary">Upload Image</Button>
								</Dropzone>
							</Grid>
							<Grid item xs={12}>
								<FormControl>
									<InputLabel htmlFor="costInput">Kosten</InputLabel>
									<Input
										id="costInput"
										value={this.state.cost}
										onChange={this.handleChange('cost').bind(this)}
										endAdornment={<InputAdornment position="end">€</InputAdornment>}
									/>
								</FormControl>
							</Grid>
						</Grid>
					</form>
				</DialogContent>
				<DialogActions>
					<Button onClick={this.closeDialog.bind(this)} color="primary">
						Cancel
					</Button>
					<Button onClick={this.fireUpdate.bind(this)} color="primary">
						Ok
					</Button>
				</DialogActions>
			</Dialog>
		</div>
		
		
	}
}

export default withMobileDialog()(ProductForm)