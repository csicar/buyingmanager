import { CardMedia, IconButton, Icon } from '@material-ui/core';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import DeleteIcon from '@material-ui/icons/Delete';
import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import Product from './Product.jsx';
import ProductForm from './ProductForm.jsx';
import RequirementEditDialog from './RequirementEditDialog.jsx'
import { formatPriority } from '../priority.js';
import EditIcon from '@material-ui/icons/Edit';


// Task component - represents a single todo item

export default class Requirement extends Component {
	constructor(props) {
		super(props);
		this.state = {
			defaultProduct: {name:"", cost:"", priority:0.5, url:""},
			dialogOpen: false
		}
	}

	addProduct({name, cost, priority, url, _id}, file) {
		Meteor.call('products.insert', name, cost, priority, url, this.props.requirement._id, file);
	}

	removeRequirement() {
		Meteor.call('requirements.remove', this.props.requirement._id);
	}

	editRequirement(requirement) {
		this.setState({dialogOpen: false})
		Meteor.call('requirements.edit', requirement)
	}

	cancelEditRequirement() {
		this.setState({dialogOpen: false})
	}

	openDialog() {
		this.setState({dialogOpen: true})
	}

	getProducts() {
		const {products, requirement} = this.props;
		return products.filter(it => it.requirement === requirement._id)
	}

	getSelectedImage() {
		const { requirement} = this.props;
		const products = this.getProducts();
		if (!products) return;
		const matchingProduct = products.find(it => it._id === requirement.resolutionId)
		if (!matchingProduct) return;
		return matchingProduct.image
	}

	renderProducts() {
		return this.getProducts().map(product =>
			<Product product={product} requirement={this.props.requirement} key={product._id}/>
		)
	}

	render() {
		return (
			<Card>
				<CardHeader
					onDoubleClick={this.openDialog.bind(this)}
					title={this.props.requirement.name}
					subheader={formatPriority(this.props.requirement.priority)}
					action={[
						<IconButton onClick={this.removeRequirement.bind(this)}><DeleteIcon /></IconButton>,
						<IconButton onClick={this.openDialog.bind(this)}><EditIcon /></IconButton>
					]}
				/>
				<RequirementEditDialog
					model={this.props.requirement}
					dialogOpen={this.state.dialogOpen}
					handleChange={this.editRequirement.bind(this)}
					handleClose={this.cancelEditRequirement.bind(this)}
					/>
				{this.getSelectedImage() && <CardMedia
					style={{paddingTop: '56%', height:0}}
					image={this.getSelectedImage()}
				/>}
				<CardContent>
					<ProductForm asEdit={false} product={this.state.defaultProduct} onUpdate={this.addProduct.bind(this)}/>
					<div className="mt-3">
						{this.renderProducts()}
					</div>
				</CardContent>
			</Card>
		)
	}
}

